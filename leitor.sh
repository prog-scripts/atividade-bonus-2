#!/bin/bash

source func.sh
read -p "Digite um nome de arquivo: " arq
read -p "Deseja exibir o arquivo por linhas ou por colunas? (l-linha, c-coluna): " o

if [ $o == l ]; then
	$Linha "$arq"
fi

if [ $o == c ]; then
	$Col "$arq"
fi
